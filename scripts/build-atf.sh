#!/bin/bash

clone_atf() {
    if [ ! -d "atf-stm32mp" ]; then
        git clone git@github.com:OneKiwiEmbedded/atf-stm32mp.git -b onekiwi-mp13-v2.6-stm32mp-r2.1
    fi
}

build_atf_mp13() {
    #build_atf_release_mp13
    build_atf_debug_mp13
}

build_atf_release_mp13() {
    source ${ENV_SETUP}
    cd atf-stm32mp
    TFA_BL32=build/stm32mp1/release/bl2.bin
    #TFA_FW=build/stm32mp1/release/fdts/${DEVICE_NAME}-bl2.dtb
    make distclean
    unset -v CFLAGS LDFLAGS

    make PLAT=stm32mp1 ARCH=aarch32 ARM_ARCH_MAJOR=7 STM32MP13=1 DTB_FILE_NAME=${DEVICE_NAME}.dtb STM32MP_SDMMC=1 STM32MP_EMMC=1 STM32MP_USB_PROGRAMMER=1
    cp build/stm32mp1/release/tf-a-${DEVICE_NAME}.stm32 ../output/tfa-usb.stm32

    # build FW_CONFIG
    make ARM_ARCH_MAJOR=7 ARCH=aarch32 PLAT=stm32mp1 AARCH32_SP=optee DTB_FILE_NAME=${DEVICE_NAME}.dtb bl32 dtbs
    
    
    make PLAT=stm32mp1 ARCH=aarch32 ARM_ARCH_MAJOR=7 STM32MP13=1 STM32MP_SDMMC=1 STM32MP_EMMC=1 STM32MP_USB_PROGRAMMER=1 DTB_FILE_NAME=${DEVICE_NAME}.dtb BL33=${ROOTDIR}/output/u-boot-nodtb.bin BL33_CFG=${ROOTDIR}/output/u-boot.dtb BL32=${TFA_BL32} fip
    #cp build/stm32mp1/release/fip.bin ../output
}

build_atf_debug_mp13() {
    source ${ENV_SETUP}
    cd atf-stm32mp
    TFA_BL32=build/stm32mp1/debug/bl2.bin
    #TFA_FW=build/stm32mp1/debug/fdts/${DEVICE_NAME}-bl2.dtb
    make distclean
    unset -v CFLAGS LDFLAGS

    make LOG_LEVEL=40 DEBUG=1 PLAT=stm32mp1 ARCH=aarch32 ARM_ARCH_MAJOR=7 STM32MP13=1 DTB_FILE_NAME=${DEVICE_NAME}.dtb STM32MP_SDMMC=1 STM32MP_EMMC=1 STM32MP_USB_PROGRAMMER=1
    cp build/stm32mp1/debug/tf-a-${DEVICE_NAME}.stm32 ../output/tfa-usb.stm32

    # build FW_CONFIG
    make LOG_LEVEL=40 DEBUG=1 ARM_ARCH_MAJOR=7 ARCH=aarch32 PLAT=stm32mp1 AARCH32_SP=optee DTB_FILE_NAME=${DEVICE_NAME}.dtb bl32 dtbs
    
    
    make LOG_LEVEL=40 DEBUG=1 PLAT=stm32mp1 ARCH=aarch32 ARM_ARCH_MAJOR=7 STM32MP13=1 STM32MP_SDMMC=1 STM32MP_EMMC=1 STM32MP_USB_PROGRAMMER=1 DTB_FILE_NAME=${DEVICE_NAME}.dtb BL33=${ROOTDIR}/output/u-boot-nodtb.bin BL33_CFG=${ROOTDIR}/output/u-boot.dtb BL32=${TFA_BL32} fip
    cp build/stm32mp1/debug/fip.bin ../output
}

build_atf_mp15() {
    source ${ENV_SETUP}
    cd atf-stm32mp
    make distclean
    unset -v CFLAGS LDFLAGS
    make PLAT=stm32mp1 ARCH=aarch32 ARM_ARCH_MAJOR=7 AARCH32_SP=sp_min STM32MP15=1 DTB_FILE_NAME=${DEVICE_NAME}.dtb STM32MP_SDMMC=1 STM32MP_EMMC=1 STM32MP_USB_PROGRAMMER=1
    cp build/stm32mp1/release/tf-a-${DEVICE_NAME}.stm32 ../output/tfa-usb.stm32

    make PLAT=stm32mp1 ARCH=aarch32 ARM_ARCH_MAJOR=7 AARCH32_SP=sp_min STM32MP_SDMMC=1 STM32MP_EMMC=1 STM32MP_USB_PROGRAMMER=1 DTB_FILE_NAME=${DEVICE_NAME}.dtb BL33=${ROOTDIR}/output/u-boot-nodtb.bin BL33_CFG=${ROOTDIR}/output/u-boot.dtb fip
    cp build/stm32mp1/release/fip.bin ../output
}
build_atf() {
    if [[ "$DEVICE_NAME" == *"stm32mp13"* ]]; then
        echo "stm32mp13"
        build_atf_mp13
    fi
    if [[ "$DEVICE_NAME" == *"stm32mp15"* ]]; then
        echo "stm32mp15"
        build_atf_mp15
    fi
}

source ./scripts/build-sdk.sh
clone_atf
build_atf