#!/bin/bash

clone_optee() {
    if [ ! -d "optee_os-stm32mp" ]; then
        git clone git@github.com:OneKiwiEmbedded/optee_os-stm32mp.git -b onekiwi-mp13-3.16.0-stm32mp-r2.1
    fi
}

build_optee() {
    cd optee_os-stm32mp
    #echo "sys: ${SDKTARGETSYSROOT}"
    #unset -v CFLAGS LDFLAGS 
    source ${ENV_SETUP}
    make distclean
    unset -v CFLAGS LDFLAGS
    make PLATFORM=stm32mp1 CFG_EMBED_DTB_SOURCE_FILE=${DEVICE_NAME}.dts CFG_TEE_CORE_LOG_LEVEL=2 CFLAGS32=--sysroot=${SDKTARGETSYSROOT} O=build all
    #comp-cflagscore=--sysroot=$SDKTARGETSYSROOT
    #make PLATFORM=stm32mp1 CFG_EMBED_DTB_SOURCE_FILE=${DEVICE_NAME}.dts CFG_TEE_CORE_LOG_LEVEL=2 comp-cflagscore=--sysroot=${SDKTARGETSYSROOT} O=build all
}

source ./scripts/build-sdk.sh

clone_optee
build_optee